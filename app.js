const express = require('express');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const moment = require('moment');
const path = require('path');

var { mongoose } = require('./db/mongoose');
var { Post } = require('./models/post');
var { Event } = require('./models/event');
var { Hardware } = require('./models/hardware');

const app = express();

app.set('view engine', 'hbs');
hbs.registerHelper('formatDate', (object) => {  // hbs helper
    if (!object) { return ""; }
    var date = moment(object.Date);
    return date.format('LL');
});
hbs.registerPartials(path.join(__dirname, '/views/partials'));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    Post.find().sort({ Date: -1 }).limit(6).then((posts) => {
        res.render('index.hbs', { posts });
    }, (e) => {
        res.render('index.hbs');
    });
});
app.get('/events', (req, res) => {
    Event.find().sort({ StartDate: -1 }).then((events) => {
        res.render('events.hbs', { events });
    }, (e) => {
        res.status(400).send(e);
    });
});
app.get('/news', (req, res) => {
    Post.find().then((posts) => {
        res.render('news.hbs', { posts });
    }, (e) => {
        res.status(400).send(e);
    });
});
app.get('/hwlibrary', (req, res) => {
    Hardware.find().then((hardware) => {
        res.render('hwlibrary.hbs', { hardware });
    }, (e) => {
        res.status(400).send(e);
    });
});

app.get('/membership', (req, res) => {
    res.render('membership.hbs');
});
app.get('/societies', (req, res) => {
    res.render('societies.hbs');
});
app.get('/timeline', (req, res) => {
    res.render('achievements.hbs');
});
app.get('/execom', (req, res) => {
    res.render('execom.hbs');
});

const hardware = require('./data');
app.get('/populate', (req, res) => {
    let count=0;
    for (var key in hardware.hardware) {
        var event = new Hardware(hardware.hardware[key]);
        event.save().then((doc) => {
            count++;
        }, (e) => {
            res.status(400).send(e);
        })
    }
    res.send({count});
    // res.send(posts);
});

app.post('/posts', (req, res) => {
    // res.send(req.body);
    var post = new Post({
        Title: req.body.title,
        Author: req.body.author || 'admin',
        Date: req.body.date ? moment(req.body.date).format() : moment().format(),
        ImageURL: req.body.imageurl || './assets/images/ieee_mb_blue_1.gif',
        Content: req.body.content,
    });
    post.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    })

});
app.post('/events', (req, res) => {
    // res.send(req.body);
    var event = new Event({
        Title: req.body.title,
        Description: req.body.description || "",
        StartDate: req.body.start ? moment(req.body.start).format() : null,
        EndDate: req.body.end ? moment(req.body.end).format() : null,
        ImageURL: req.body.imageurl || './assets/images/ieee_mb_blue_1.gif',
        EventPage: req.body.eventpageurl || '#',
    });
    event.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    })

});

app.listen(3000, () => console.log('Example app listening on port 3000!'));
