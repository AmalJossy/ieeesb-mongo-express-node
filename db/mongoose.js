var mongoose = require('mongoose');
mongoose.Promise=global.Promise;
const dbAddress = process.env.DB_HOST || "127.0.0.1";
const dbPort = process.env.DB_PORT || 27017;
const dbURL=`mongodb://${dbAddress}:${dbPort}/IEEE`;
mongoose.connect(dbURL);
module.exports={
    mongoose
}