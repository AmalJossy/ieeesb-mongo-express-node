var mongoose = require('mongoose');
var Event= mongoose.model('Event',{
    Title:{
        type: String,
        required:true,
        minlength:1,
        trim:true
    },
    Description:{
        type: String,
        trim:true
    },
    StartDate:{
        type:Date,
        required:true
    },
    EndDate:{
        type:Date
    },
    EventPage: {
        type:String,
        default: '#'
    },
    ImageURL:{
        type: String
    }
});
module.exports={
    Event
}