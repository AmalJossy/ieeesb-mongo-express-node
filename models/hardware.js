var mongoose = require('mongoose');
var Hardware= mongoose.model('Hardware',{
    ItemName:{
        type: String,
        required:true,
        minlength:1,
        trim:true
    },
    ImageURL:{
        type: String
    },
    DataSheet: {
        type: String,
        default: '#'
    },
    Available:{
        type: Number,
        default:1
    }
});
module.exports={
    Hardware
}