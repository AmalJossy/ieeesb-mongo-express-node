var mongoose = require('mongoose');
var Post= mongoose.model('Post',{
    Title:{
        type: String,
        required:true,
        minlength:1,
        trim:true
    },
    Date:{
        type:Date,
        default: Date.now()
    },
    Author:{
        type:String,
        default: 'Admin'
    },
    Content: {
        type:String,
        required: true

    },
    ImageURL:{
        type: String
    }
});
module.exports={
    Post
}