$('.hero>').particleground();
const drawWedge = function (element, height, direction) {
    if (direction == "left") {
        element.innerHTML = `<svg width="${height * 2}" height="${height}">
        <polygon points="0 0, 0 ${height}, ${height * 1.5} ${height}, ${height * 2} ${height * .5},${height * 2} 0" /> Sorry, your browser does not support inline SVG.
    </svg>`;
    }
    else {
        element.innerHTML = `<svg width="${height * 2}" height="${height}">
        <polygon points="0 ${height * .5}, 0 ${height}, ${height * 2} ${height}, ${height * 2} 0,${height * .5} 0" /> Sorry, your browser does not support inline SVG.
    </svg>`;
    }
}
var wedgeleft = document.getElementsByClassName("wedge-left")[0];
if (wedgeleft) {
    drawWedge(wedgeleft, wedgeleft.dataset.height, 'left');
}
var wedgeright = document.getElementsByClassName("wedge-right")[0];
if (wedgeright) {
    drawWedge(wedgeright, wedgeright.dataset.height, 'right')
};